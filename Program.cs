﻿using ACHelperBot.Lib;
using ACHelperBot.Lib.MessageFinders;
using ACHelperBot.Models;
using Discord;
using Discord.WebSocket;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot {
    class Program {
        private DiscordSocketClient _client;

        public static void Main(string[] args) {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            new Program().MainAsync().GetAwaiter().GetResult();
		}

        /// <summary>
        /// Setup event handlers and connect to discord
        /// </summary>
        /// <returns></returns>
		public async Task MainAsync() {
            _client = new DiscordSocketClient();

            _client.Log += Log;
            _client.MessageReceived += _client_MessageReceived;

            var token = Environment.GetEnvironmentVariable("ACHB_DISCORD_TOKEN");

            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();

            // Block this task until the program is closed.
            await Task.Delay(-1);
        }

        /// <summary>
        /// Handle message received events, this can be in a channel or direct message
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private async Task _client_MessageReceived(SocketMessage arg) {
            if (arg.Author.IsBot)
                return;
            await CheckForDecalExports(arg);
            await CheckForErrorImages(arg);
        }

        private async Task CheckForDecalExports(SocketMessage arg) {
            var decalExportLinks = LinkFinder.FindDecalExportLinks(arg);
            var decalExportAttachments = AttachmentFinder.FindDecalExportAttachments(arg);
            var possibleDecalExportLinks = decalExportLinks.Concat(decalExportAttachments);

            if (possibleDecalExportLinks.Count() == 0) {
                ChatTextFinder.FindMessageHandlers(arg);
                return;
            }

            await Task.Run(async () => {
                await DownloadAndCheckDecalExports(arg, possibleDecalExportLinks);
            });
        }

        private async Task CheckForErrorImages(SocketMessage arg) {
            var imageAttachments = AttachmentFinder.FindImageAttachments(arg);
            var imageLinks = LinkFinder.FindImageLinks(arg);
            var possibleImages = imageAttachments.Concat(imageLinks);

            if (possibleImages.Count() > 0) {
                await Task.Run(async () => {
                    await DownloadAndCheckImages(arg, possibleImages);
                });
            }
        }

        private async Task DownloadAndCheckImages(SocketMessage arg, IEnumerable<string> imageAttachments) {
            foreach (var image in imageAttachments) {
                try {
                    await Task.Run(async () => {
                        try {
                            HttpClient httpClient = new HttpClient();
                            httpClient.Timeout = new TimeSpan(1, 1, 1);

                            MultipartFormDataContent form = new MultipartFormDataContent();
                            form.Add(new StringContent(Environment.GetEnvironmentVariable("ACHB_OCR_KEY")), "apikey");
                            form.Add(new StringContent("eng"), "language");
                            form.Add(new StringContent(image), "url");

                            HttpResponseMessage response = await httpClient.PostAsync("https://api.ocr.space/Parse/Image", form);
                            string strContent = await response.Content.ReadAsStringAsync();
                            Rootobject ocrResult = JsonConvert.DeserializeObject<Rootobject>(strContent);

                            var text = "";
                            if (ocrResult.OCRExitCode == 1) {
                                for (int i = 0; i < ocrResult.ParsedResults.Count(); i++) {
                                    text += ocrResult.ParsedResults[i].ParsedText;
                                }
                                text = text.Trim().Replace("\n", "").Replace("\r", " ");
                            }
                            else {
                                Console.WriteLine("ERROR: " + strContent);
                            }

                            if (!string.IsNullOrEmpty(text)) {
                                Console.WriteLine($"Found image text: {text}");
                                List<string> results = ChatTextFinder.FindResponsesForMessage(text);

                                if (results.Count > 0)
                                    await arg.Channel.SendMessageAsync(string.Join("\n", results));
                            }
                        }
                        catch (Exception ex) {
                            Console.Write(ex.ToString());
                        }
                    });
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        private async Task DownloadAndCheckDecalExports(SocketMessage arg, IEnumerable<string> possibleDecalExportLinks) {
            List<string> textBlobs = await LinkDownloader.DownloadAll(possibleDecalExportLinks);
            foreach (var blob in textBlobs) {
                if (string.IsNullOrWhiteSpace(blob))
                    continue;

                var parseException = false;
                DecalExport decalExport = null;

                try {
                    decalExport = new DecalExport(blob);
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.ToString());
                    parseException = true;
                }

                if (!parseException && decalExport != null && decalExport.DidParse) {
                    var runner = new DecalExportRuleRunner(decalExport);
                    runner.Run();
                    if (runner.HasErrorsOrWarnings()) {
                        await arg.Channel.SendMessageAsync(runner.GetSuggestionsMessage());
                    }
                    else {
                        await arg.Channel.SendMessageAsync($"That decal export looks good to me.  If there is an issue that is present in the export please let me know so I can add a new rule to check.\n\nTips:\n- Press Alt+Enter at game start to *exit* fullscreen.\n- Make sure your client/dat files are up-to-date.\n- Launch ThwargLauncher as administrator.\n- Make sure the server name in thwarglauncher *exactly* matches the ingame server name.\n- If your game closes during character creation, close thwarglauncher during creation.\n- Close all bloatware in system tray: audio/gfx managers especially.\n- Make sure you have vs2005 runtime from <https://www.decaldev.com/> \n- Make sure you follow *all* the steps at <https://www.gdleac.com/#installing> or <https://emulator.ac/how-to-play/> ");
                    }
                }
                else {
                    if (decalExport != null)
                        Console.WriteLine($"Could not parse export: {decalExport.ParseError}");
                }
            }
        }

        /// <summary>
        /// Log events to console
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private Task Log(LogMessage msg) {
			Console.WriteLine(msg.ToString());
			return Task.CompletedTask;
		}
	}
}
