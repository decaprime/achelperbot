﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACHelperBot.Lib.MessageFinders {
    internal static class AttachmentFinder {

        /// <summary>
        /// Returns all attachment urls on a message that could be decal exports
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        internal static List<string> FindDecalExportAttachments(SocketMessage arg) {
            List<string> results = new List<string>();

            foreach (var attachment in arg.Attachments) {
                // only looking at txt files and files with no extension
                if (!attachment.Filename.EndsWith(".txt") && attachment.Filename.Contains("."))
                    continue;

                // skip files larger than 100k (my decal export is ~15k with several plugins)
                if (attachment.Size > 1024 * 1024 * 100)
                    continue;

                results.Add(attachment.Url);
            }

            return results;
        }

        internal static List<string> FindImageAttachments(SocketMessage arg) {
            List<string> results = new List<string>();

            foreach (var attachment in arg.Attachments) {
                if (attachment.Filename.EndsWith(".png") || attachment.Filename.EndsWith(".jpg") || attachment.Filename.EndsWith(".gif")) {
                    results.Add(attachment.Url);
                }
            }

            return results;
        }
    }
}
