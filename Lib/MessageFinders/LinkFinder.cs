﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ACHelperBot.Lib.MessageFinders {
    internal static class LinkFinder {
        private static Regex URLRegex = new Regex(@"(https?:\/\/)?([\w\-])+\.{1}([a-zA-Z]{2,63})([\/\w-]*)*\/?\??([^#\n\r]*)?#?([^\n\r]*)", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        private static Regex PasteBinPathRegex = new Regex(@"^/[a-z0-9]+$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        /// <summary>
        /// Find any links in the message and return a list of possible decal export links
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        internal static List<string> FindDecalExportLinks(SocketMessage arg) {
            List<string> results = new List<string>();
            MatchCollection matches = URLRegex.Matches(arg.ToString());
            foreach (Match match in matches) {
                try {
                    Uri foundUri = new Uri(match.Value);

                    // currently only looking for pastebin
                    if (foundUri.Host.ToLower() == "pastebin.com" && PasteBinPathRegex.IsMatch(foundUri.AbsolutePath)) {
                        results.Add($"https://pastebin.com/raw{foundUri.AbsolutePath}");
                    }
                }
                catch { }
            }

            return results;
        }

        /// <summary>
        /// find direct links to images
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        internal static List<string> FindImageLinks(SocketMessage arg) {
            List<string> results = new List<string>();
            MatchCollection matches = URLRegex.Matches(arg.ToString());
            foreach (Match match in matches) {
                try {
                    Uri foundUri = new Uri(match.Value);

                    // currently only looking for pastebin
                    if (match.Value.EndsWith(".png") || match.Value.EndsWith(".jpg") || match.Value.EndsWith(".gif")) {
                        results.Add(match.Value);
                    }
                    else if (foundUri.Host.ToLower() == "imgur.com") {
                        results.Add($"https://i.imgur.com{foundUri.AbsolutePath}.jpg");
                    }
                }
                catch { }
            }

            return results;
        }
    }
}
