﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ACHelperBot.Lib.MessageFinders {
    class ChatTextFinder {
        static RegexOptions DefaultRegexOptions = RegexOptions.IgnoreCase | RegexOptions.Compiled;

        static Dictionary<Regex, string> ChatHandlers = new Dictionary<Regex, string>() {
            // DirectX crashes
            {
                new Regex(@"(?=(.*(error|crash|problem|virindi).*)).*(direct ?(x|3d)|dx).*", DefaultRegexOptions),
                "Try right clicking `C:\\Turbine\\Asheron's Call\\acclient.exe` -> Compatibility -> Check `Disable fullscreen optimizations`\n- You may need to install dxwebsetup from <https://files.treestats.net/>\n- If all else fails, you may need to run the game in windowed mode. Press Alt+Enter before entering the game."
            },
            // vcredist
            {
                new Regex(@"(?=.*(error|trouble|problem).*)(?=.*(virindi|vtank|bundle|install).*).*(2005|vcredist).*", DefaultRegexOptions),
                "Make sure you have installed vcredist_x86 from <https://files.treestats.net/>"
            },
            // client closes during character creation
            {
                new Regex(@"(can'?t stay logged|finish making char|(make.*char|creat.*char|char.*creat).*(crash|kick|d\/?c|disconnect)|(crash|kick|d\/?c|disconnect).*(make.*char|creat.*char|char.*creat))", DefaultRegexOptions),
                "If you are using thwarglauncher advanced mode, either close it during character creation or use simple mode to create characters.\n- Make sure the servername in thwarglauncher exactly matches the server name on the character select screen.\n- Make sure that thwargfilter.dll has been added to decal."
            },
            // client keeps closing
            {
                new Regex(@"client keeps (closing|crashing)", DefaultRegexOptions),
                "If you are using thwarglauncher advanced mode, either close it during character creation or use simple mode to create characters.\n- Make sure the servername in thwarglauncher exactly matches the server name on the character select screen.\n- Make sure that thwargfilter.dll has been added to decal."
            },
            // dat files
            {
                new Regex(@"(Can'?t open the data files|Check that they exist and that you have permission to write to them)", DefaultRegexOptions),
                "Try disabling compatibility mode by right clicking acclient.exe in `C:\\Turbine\\Asheron's Call\\`. You may also need to right click and choose properties on the .dat files in the same directory. Properties -> Unblock"
            }
        };

        /// <summary>
        /// Find any pattern matches and reply with the handler text
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        internal static void FindMessageHandlers(SocketMessage arg) {
            List<string> results = FindResponsesForMessage(arg.ToString());

            if (results.Count > 0)
                arg.Channel.SendMessageAsync(string.Join("\n", results));
        }

        internal static List<string> FindResponsesForMessage(string v) {
            List<string> results = new List<string>();
            foreach (var kv in ChatHandlers) {
                if (kv.Key.IsMatch(v)) {
                    results.Add($"- {kv.Value}");
                }
            }
            return results;
        }
    }
}
