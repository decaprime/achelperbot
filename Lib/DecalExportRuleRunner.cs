﻿using ACHelperBot.Models;
using ACHelperBot.Rules.Decal;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACHelperBot.Lib {
    class DecalExportRuleRunner {
        private DecalExport decalExport;

        private List<string> errors = new List<string>();
        private List<string> warnings = new List<string>();

        public DecalExportRuleRunner(DecalExport decalExport) {
            this.decalExport = decalExport;
        }

        // surely we could do this better...
        public void Run() {
            var sanityCheck = new DecalSanityCheck();

            sanityCheck.Passes(decalExport);
            errors = errors.Concat(sanityCheck.GetErrors()).ToList();
            warnings = warnings.Concat(sanityCheck.GetWarnings()).ToList();

            // bail early if decal sanity check *fails*. warnings are OK
            if (errors.Count > 0)
                return;

            // generic rules
            var rules = new List<IDecalExportRule>();

            rules.Add(new PluginSanityCheck());
            rules.Add(new ThwargFilterCheck());
            rules.Add(new UtilityBeltCheck());
            rules.Add(new GoArrowCheck());
            rules.Add(new RadarAddonCheck());

            foreach (var rule in rules) {
                var passed = rule.Passes(decalExport);
                errors = errors.Concat(rule.GetErrors()).ToList();
                warnings = warnings.Concat(rule.GetWarnings()).ToList();
            }
        }

        internal string GetSuggestionsMessage() {
            var message = "";

            if (errors.Count > 0) {
                message += "Errors:\n";
                foreach (var error in errors) {
                    message += $"  - {error}\n";
                }

                if (warnings.Count > 0)
                    message += "\n";
            }

            if (warnings.Count > 0) {
                message += "Warnings:\n";
                foreach (var warning in warnings) {
                    message += $"  - {warning}\n";
                }
            }

            return message;
        }

        internal bool HasErrorsOrWarnings() {
            return errors.Count > 0 || warnings.Count > 0;
        }
    }
}
