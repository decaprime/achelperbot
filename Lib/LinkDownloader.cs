﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Lib {
    static class LinkDownloader {
        internal static async Task<List<string>> DownloadAll(IEnumerable<string> possibleDecalExportLinks) {
            List<string> results = new List<string>();

            foreach (var link in possibleDecalExportLinks) {
                results.Add(await DownloadFileAsync(link));
            }

            return results;
        }

        internal static async Task<string> DownloadFileAsync(string address) {
            try {
                string contents = null;
                await Task.Run(() => {
                    using (WebClient webClient = new WebClient()) {
                        contents = webClient.DownloadString(new Uri(address)).Trim();
                    }
                });
                return contents;
            }
            catch (Exception) {
                Console.WriteLine($"Failed to download: {address}");
            }

            return null;
        }

        internal static async Task<string> DownloadImageAsync(string address, string path) {
            try {
                await Task.Run(() => {
                    using (WebClient webClient = new WebClient()) {
                        webClient.DownloadFile(address, path);
                    }
                });
            }
            catch (Exception) {
                Console.WriteLine($"Failed to download: {address}");
            }

            return null;
        }
    }
}
