﻿using ACHelperBot.Lib;
using ACHelperBot.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Rules.Decal {
    class UtilityBeltCheck : IDecalExportRule {
        List<string> errors = new List<string>();
        List<string> warnings = new List<string>();

        DecalExport decalExport;
        private DecalComponent ubFilter;

        public class GitLabTagData {
            public string name = "";
            public string tag_name = "";
            public DateTime created_at = DateTime.MinValue;
            public DateTime released_at = DateTime.MinValue;
            public string description = "";

        }

        public List<string> GetErrors() {
            return errors;
        }

        public List<string> GetWarnings() {
            return warnings;
        }

        public bool Passes(DecalExport decalExport) {
            this.decalExport = decalExport;

            if (!CheckExists())
                return true;

            CheckVTank();
            CheckVVS();
            CheckDotNet();
            CheckVersion().Wait();

            return GetWarnings().Count == 0 && GetErrors().Count == 0;
        }

        /// <summary>
        /// Checks if the plugin is loaded
        /// </summary>
        /// <returns></returns>
        private bool CheckExists() {
            ubFilter = decalExport.Components.Where(p => p.Name == "UtilityBelt").FirstOrDefault();

            return ubFilter != null;
        }

        /// <summary>
        /// Checks that VVS is installed and enabled
        /// </summary>
        private void CheckVVS() {
            var vvs = decalExport.Services.Where(p => p.Name == "Virindi View Service Bootstrapper").FirstOrDefault();

            if (vvs == null || vvs.Enabled != 1) {
                errors.Add($"UtilityBelt requires Virindi View Service. Make sure it is installed and enabled.");
                return;
            }

            Console.WriteLine($"Passed: UtilityBeltChecks.CheckVVS");
        }

        /// <summary>
        /// Checks that VTank is installed and enabled
        /// </summary>
        private void CheckVTank() {
            var vTank = decalExport.Plugins.Where(p => p.Name == "Virindi Tank").FirstOrDefault();

            if (vTank == null || vTank.Enabled != 1) {
                errors.Add($"UtilityBelt requires the VTank plugin. Make sure it is installed and enabled.");
                return;
            }

            Console.WriteLine($"Passed: UtilityBeltChecks.CheckVTank");
        }

        /// <summary>
        /// Check for latest version
        /// </summary>
        /// <returns></returns>
        private async Task<bool> CheckVersion() {
            try {
                var contents = await LinkDownloader.DownloadFileAsync("https://gitlab.com/api/v4/projects/10819053/releases");
                var tags = JsonConvert.DeserializeObject<GitLabTagData[]>(contents);

                if (tags.Count() == 0)
                    return true;

                Version currentVersion = new Version(ubFilter.Version.ToString());
                Version releaseVersion = new Version(tags[0].tag_name.Replace("release-", ""));

                if (currentVersion < releaseVersion) {
                    warnings.Add($"UtilityBelt plugin is out of date. Expected:>={releaseVersion} Actual:{currentVersion} Grab the latest version at <https://utilitybelt.gitlab.io/>");
                    return false;
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.ToString());
            }

            Console.WriteLine($"Passed: UtilityBeltChecks.CheckVersion");

            return true;
        }

        /// <summary>
        /// Checks that dotnet 3.5 is installed
        /// </summary>
        /// <returns></returns>
        private bool CheckDotNet() {
            if (!decalExport.DotNetFrameworks.IsInstalled("3.5")) {
                errors.Add($"UtilityBelt requires dotnet 3.5. Download it here: <https://dotnet.microsoft.com/download/dotnet-framework/net35-sp1>");
                return false;
            }

            Console.WriteLine($"Passed: UtilityBeltChecks.CheckDotNet");

            return true;
        }
    }
}
