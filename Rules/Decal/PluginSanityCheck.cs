﻿using ACHelperBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ACHelperBot.Rules.Decal {
    class PluginSanityCheck : IDecalExportRule {
        List<string> errors = new List<string>();
        List<string> warnings = new List<string>();

        DecalExport decalExport;

        public List<string> GetErrors() {
            return errors;
        }

        public List<string> GetWarnings() {
            return warnings;
        }

        public bool Passes(DecalExport decalExport) {
            this.decalExport = decalExport;

            CheckDlls();
            CheckLocations();

            return true;
        }

        /// <summary>
        /// Check that plugin / filter dlls exist
        /// </summary>
        /// <returns></returns>
        private bool CheckDlls() {
            List<string> missingDlls = new List<string>();
            var components = decalExport.Plugins.Concat(decalExport.NetworkFilters);

            foreach (var component in components) {
                if (component.NoDll) {
                    missingDlls.Add($"{component.ComponentType}:{component.Name}");
                }
            }

            if (missingDlls.Count > 0) {
                errors.Add($"The following decal components are missing DLLs (either moved or deleted). You will need to reinstall them: {string.Join(", ", missingDlls)}");
                return false;
            }

            Console.WriteLine($"Passed: PluginSanityCheck.CheckDlls");

            return true;
        }

        /// <summary>
        /// Check that plugins are not installed to weird folders (currently only checks downloads)
        /// </summary>
        /// <returns></returns>
        private bool CheckLocations() {
            List<string> downloadDlls = new List<string>();
            List<string> acDlls = new List<string>();
            List<string> decalDlls = new List<string>();
            var components = decalExport.Plugins.Concat(decalExport.NetworkFilters);

            foreach (var component in components) {
                if (string.IsNullOrEmpty(component.Location))
                    continue;

                if (component.Location.ToLower().Contains("downloads")) {
                    downloadDlls.Add($"{component.ComponentType}:{component.Name}");
                }

                if (component.Location.ToLower().Contains(decalExport.DecalSettings.PortalPath.ToLower())) {
                    acDlls.Add($"{component.ComponentType}:{component.Name}");
                }

                if (component.ComponentType == DecalComponentType.Plugin && component.Location.ToLower().StartsWith(@"c:\program files (x86)\decal 3.0\") && component.Name != "Decal Hotkey System") {
                    decalDlls.Add(component.Name);
                }
            }

            if (downloadDlls.Count > 0) {
                warnings.Add($"The following decal components appear to be in your downloads directory, you should move them somewhere more permanent like `C:\\Games\\Decal Plugins\\<Plugin>\\`: {string.Join(", ", downloadDlls)}");
                return false;
            }

            if (acDlls.Count > 0) {
                warnings.Add($"The following decal components appear to be in your ac install directory, you should move them somewhere with their own directory, like `C:\\Games\\Decal Plugins\\<Plugin Name>\\<Plugin>.dll`: {string.Join(", ", acDlls)}");
                return false;
            }

            if (decalDlls.Count > 0) {
                warnings.Add($"The following decal plugins appear to be installed to your decal directory, you should move them somewhere with their own directory, like `C:\\Games\\Decal Plugins\\<Plugin Name>\\<Plugin>.dll`: {string.Join(", ", decalDlls)}");
                return false;
            }

            Console.WriteLine($"Passed: PluginSanityCheck.CheckLocations");

            return true;
        }
    }
}
