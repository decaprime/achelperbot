﻿using ACHelperBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ACHelperBot.Rules.Decal {
    class ThwargFilterCheck : IDecalExportRule {
        List<string> errors = new List<string>();
        List<string> warnings = new List<string>();

        DecalExport decalExport;

        public List<string> GetErrors() {
            return errors;
        }

        public List<string> GetWarnings() {
            return warnings;
        }

        public bool Passes(DecalExport decalExport) {
            this.decalExport = decalExport;

            CheckThwargFilterRegistered();

            return errors.Count == 0 && warnings.Count == 0;
        }

        /// <summary>
        /// Check that thwargfilter is registered / enabled.
        /// </summary>
        /// <returns></returns>
        private bool CheckThwargFilterRegistered() {
            var tf = decalExport.NetworkFilters.Where(f => f.Name == "ThwargFilter").FirstOrDefault();

            if (tf == null || tf.Enabled != 1) {
                warnings.Add($"If you are using ThwargLauncher you need to add ThwargFilter.dll to decal. If you are not, you can ignore this message.");
                return false;
            }

            Console.WriteLine($"Passed: ThwargFilterCheck.CheckThwargFilterRegistered");

            return true;
        }
    }
}
