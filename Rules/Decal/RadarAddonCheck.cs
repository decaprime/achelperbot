﻿using ACHelperBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ACHelperBot.Rules.Decal {
    class RadarAddonCheck : IDecalExportRule {
        List<string> errors = new List<string>();
        List<string> warnings = new List<string>();

        DecalExport decalExport;

        public List<string> GetErrors() {
            return errors;
        }

        public List<string> GetWarnings() {
            return warnings;
        }

        public bool Passes(DecalExport decalExport) {
            this.decalExport = decalExport;

            CheckRadarVersion();

            return errors.Count == 0 && warnings.Count == 0;
        }

        /// <summary>
        /// Warn the user if they are running an old (broken) radar addon
        /// </summary>
        private bool CheckRadarVersion() {
            var radarAddon = decalExport.Plugins.Where(f => f.Name == "Radar Add-on").FirstOrDefault();

            if (radarAddon != null && radarAddon.Version < new Version("1.2.0.71")) {
                warnings.Add($"You are running an old (possibly broken) version of Radar Add-on. You can update it here: <http://gdleac.com/files/RadarAddOn_v1.2.0.71.msi>");
                return false;
            }

            Console.WriteLine($"Passed: RadarAddon.CheckRadarVersion");

            return true;
        }
    }
}
