﻿using ACHelperBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACHelperBot.Rules.Decal {
    class DecalSanityCheck : IDecalExportRule {
        const string expectedDecalVersion = "2.9.7.5";

        List<string> errors = new List<string>();
        List<string> warnings = new List<string>();

        DecalExport decalExport;

        public static Dictionary<string, string> DefaultInstallPaths = new Dictionary<string, string>() {
            { "Echo Filter 2", @"C:\Program Files (x86)\Decal 3.0\DecalFilters.dll" },
            { "Character Stats Filter", @"C:\Program Files (x86)\Decal 3.0\DecalFilters.dll" },
            { "World Object Filter", @"C:\Program Files (x86)\Decal 3.0\DecalFilters.dll" },
            { "Decal FileService", @"C:\Program Files (x86)\Decal 3.0\Decal.FileService.dll" },
            { "Identify Queue Filter", @"C:\Program Files (x86)\Decal 3.0\DecalFilters.dll" },
            { "Decal Dat Service", @"C:\Program Files (x86)\Decal 3.0\decaldat.dll" },
            { "Decal .NET Lifetime Service", @"C:\Program Files (x86)\Decal 3.0\Decal.Adapter.dll" },
            { "Decal Input Service", @"C:\Program Files (x86)\Decal 3.0\decalInput.dll" },
            { "Decal Networking Service", @"C:\Program Files (x86)\Decal 3.0\decalnet.dll" },
            { "Decal D3DService", @"C:\Program Files (x86)\Decal 3.0\D3DService.dll" },
            { "Decal Render Service", @"C:\Program Files (x86)\Decal 3.0\decalrender.dll" },
            { "Decal Inject Gateway Service", @"C:\Program Files (x86)\Decal 3.0\Inject.dll" },
            { "Version 1 Plugin Surrogate", @"C:\Program Files (x86)\Decal 3.0\Inject.dll" },
            { "Prefilter Network Filter Surrogate", @"C:\Program Files (x86)\Decal 3.0\DecalFilters.dll" },
            { "Decal.Adapter Surrogate", @"C:\Program Files (x86)\Decal 3.0\Decal.Adapter.dll" },
            { "ActiveX Plugin Surrogate", @"C:\Program Files (x86)\Decal 3.0\Decal.dll" },
            { "Delay Input Action", @"C:\Program Files (x86)\Decal 3.0\decalInput.dll" },
            { "Mouse Move Input Action", @"C:\Program Files (x86)\Decal 3.0\decalInput.dll" },
            { "Restore Input Action", @"C:\Program Files (x86)\Decal 3.0\decalInput.dll" },
            { "Polled Delay Input Action", @"C:\Program Files (x86)\Decal 3.0\decalInput.dll" },
            { "Typing Input Action", @"C:\Program Files (x86)\Decal 3.0\decalInput.dll" },
            { "Event Input Action", @"C:\Program Files (x86)\Decal 3.0\decalInput.dll" },
        };

        public List<string> GetErrors() {
            return errors;
        }

        public List<string> GetWarnings() {
            return warnings;
        }

        public bool Passes(DecalExport decalExport) {
            this.decalExport = decalExport;

            if (!CheckVersion())
                return false;

            if (!CheckLocationsEnabled())
                return false;

            if (!CheckDX9_30())
                return false;

            if (!CheckInjectionMethod())
                return false;

            if (!CheckLauncherApp())
                return false;

            if (!CheckComponents())
                return false;

            if (!CheckMissingComponents())
                return false;

            CheckInstallPaths();

            return true;
        }

        /// <summary>
        /// Make sure the locations checkbox was enabled
        /// </summary>
        /// <returns></returns>
        private bool CheckLocationsEnabled() {
            if (!decalExport.HasPluginLocationsEnabled) {
                errors.Add($"Please check the locations checkbox at the top of the export window during your decal export.");
                return false;
            }

            Console.WriteLine($"Passed: DecalSanityCheck.CheckLocationsEnabled");

            return true;
        }

        /// <summary>
        /// Make sure [d3dx9_30.dll] is installed
        /// </summary>
        /// <returns></returns>
        private bool CheckDX9_30() {
            if (!decalExport.D3DXLibraries.IsInstalled("d3dx9_30.dll")) {
                errors.Add($"d3dx9_30.dll is not installed.  You should install dxwebsetup from <https://files.treestats.net/>");
                return false;
            }

            Console.WriteLine($"Passed: DecalSanityCheck.CheckDX9_30");

            return true;
        }

        /// <summary>
        /// Check decal version is correct
        /// </summary>
        /// <returns></returns>
        private bool CheckVersion() {
            if (!decalExport.DecalVersion.ToString().Equals(expectedDecalVersion)) {
                errors.Add($"Decal Version should be {expectedDecalVersion}. Got: {decalExport.DecalVersion}");
                return false;
            }

            Console.WriteLine($"Passed: DecalSanityCheck.CheckVersion");

            return true;
        }

        /// <summary>
        /// Check decal injection method is set to default
        /// </summary>
        /// <returns></returns>
        private bool CheckInjectionMethod() {
            if (decalExport.DecalSettings.InjectionMethod != DecalSettings.InjectionMethodType.Timer) {
                errors.Add($"Open up Decal options and uncheck `Use Alternate Injection Method`");
                return false;
            }

            Console.WriteLine($"Passed: DecalSanityCheck.CheckInjectionMethod");

            return true;
        }

        /// <summary>
        /// Check that the LauncherApp is set to acclient.exe
        /// </summary>
        /// <returns></returns>
        private bool CheckLauncherApp() {
            if (decalExport.DecalSettings.LauncherApp != "acclient.exe") {
                errors.Add($"Bad `Launcher App` set in decal. Expected=acclient.exe Actual={decalExport.DecalSettings.LauncherApp} . Make sure your client/dat files are up to date, then run the decal installer, choose repair, select the acclient in your C:\\Turbine\\Asheron's call\\ folder when prompted by decal on first run after repair");
                return false;
            }

            return true;
        }

        /// <summary>
        /// This just checks all non plugin components for NoVersion and ErrorLoadingDLLPath errors. If either
        /// are greater than 5, we just assume decal is completely broken and the user needs to redo the installation. 
        /// </summary>
        /// <returns></returns>
        private bool CheckComponents() {
            var noVersionCount = decalExport.Components.Where(c => c.ComponentType != DecalComponentType.Plugin && c.NoVersion).Count();
            var errorLoadingDLLPathCount = decalExport.Components.Where(c => c.ErrorLoadingDLLPath).Count();
            var invalidComponents = decalExport.Components.Where(c => c.ComponentType != DecalComponentType.Plugin && c.Enabled == -1).Count();
            var validComponents = decalExport.Components.Where(c => c.ComponentType != DecalComponentType.Plugin && c.Enabled == 1).Count();

            if (noVersionCount > 5 || errorLoadingDLLPathCount > 5 || invalidComponents > 5 || validComponents == 0) {
                Console.WriteLine($"noVersionCount:{noVersionCount} errorLoaddingDLLPathCount:{errorLoadingDLLPathCount} invalidComponents:{invalidComponents} validComponents:{validComponents}");
                errors.Add($"Your decal appears to be completely broken. You may need to reinstall.  Make sure you follow *all* the steps at <https://www.gdleac.com/#installing> or <https://emulator.ac/how-to-play/>");
                return false;
            }

            Console.WriteLine($"Passed: DecalSanityCheck.CheckComponents ({noVersionCount}, {errorLoadingDLLPathCount})");

            return true;
        }

        /// <summary>
        /// Check for any missing decal components
        /// </summary>
        /// <returns></returns>
        private bool CheckMissingComponents() {
            var missingComponents = new List<string>();
            foreach (var kv in DefaultInstallPaths) {
                var component = decalExport.Components.Where(c => c.Name == kv.Key).FirstOrDefault();
                if (component == null) {
                    missingComponents.Add(kv.Value);
                }
            }

            if (missingComponents.Count > 0) {
                errors.Add($"Decal is missing the following required components: {string.Join(", ", missingComponents)}. You need to reinstall decal.");
                return false;
            }

            Console.WriteLine($"Passed: DecalSanityCheck.CheckMissingComponents");

            return true;
        }

        /// <summary>
        /// Check and warn the user if there are non-default install paths
        /// </summary>
        private void CheckInstallPaths() {
            if (!decalExport.DecalSettings.PortalPath.Equals(@"C:\Turbine\Asheron's Call")) {
                warnings.Add($"Your AC install path is not set to default.  While this is not techinically wrong, it can potentially lead to other issues. Should be: `C:\\Turbine\\Asheron's Call`");
            }

            var nonDefaultInstallComponents = new List<string>();
            foreach (var kv in DefaultInstallPaths) {
                var component = decalExport.Components.Where(c => c.Name == kv.Key).FirstOrDefault();
                if (component != null && component.Location != kv.Value && !component.NoDll) {
                    nonDefaultInstallComponents.Add($"{component.ComponentType}:{kv.Key}");
                }
            }

            if (nonDefaultInstallComponents.Count > 0) {
                warnings.Add($"The following decal components are installed to a non-default path. While this is not technically wrong, it can potentially lead to other issues: {string.Join(", ", nonDefaultInstallComponents)}.");
                return;
            }

            Console.WriteLine($"Passed: DecalSanityCheck.CheckInstallPaths");
        }
    }
}
