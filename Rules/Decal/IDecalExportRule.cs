﻿using ACHelperBot.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACHelperBot.Rules.Decal {
    interface IDecalExportRule {
        public bool Passes(DecalExport decalExport);

        public List<string> GetErrors();
        public List<string> GetWarnings();
    }
}
