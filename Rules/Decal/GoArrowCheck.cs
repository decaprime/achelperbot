﻿using ACHelperBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ACHelperBot.Rules.Decal {
    class GoArrowCheck : IDecalExportRule {
        List<string> errors = new List<string>();
        List<string> warnings = new List<string>();

        DecalExport decalExport;

        public List<string> GetErrors() {
            return errors;
        }

        public List<string> GetWarnings() {
            return warnings;
        }

        public bool Passes(DecalExport decalExport) {
            this.decalExport = decalExport;

            if (!CheckConflicts())
                return false;

            CheckInstallLocation();

            return errors.Count == 0 && warnings.Count == 0;
        }

        /// <summary>
        /// Warn the user if goarrow is installed to decal or ac directory
        /// </summary>
        private bool CheckInstallLocation() {
            var goArrow = decalExport.Plugins.Where(f => f.Name == "GoArrow" || f.Name == "GoArrow (VVS Edition)").FirstOrDefault();

            if (goArrow != null && !string.IsNullOrEmpty(goArrow.Location) && (goArrow.Location.Contains(decalExport.DecalSettings.PortalPath) || goArrow.Location.Contains("Program Files"))) {
                errors.Add($"Your {goArrow.Name} plugin is installed in a weird directory. You should make a new directory `C:\\Games\\Decal Plugins\\GoArrow\\` and install it there.");
                return false;
            }

            Console.WriteLine($"Passed: GoArrowCheck.CheckGoArrowInstallLocation");

            return true;
        }

        /// <summary>
        /// Check that both pre-vvs goarrow and vvs-goarrow are both not enabled
        /// </summary>
        /// <returns></returns>
        private bool CheckConflicts() {
            var originalGoArrow = decalExport.Plugins.Where(f => f.Name == "GoArrow").FirstOrDefault();
            var vvsGoArrow = decalExport.Plugins.Where(f => f.Name == "GoArrow (VVS Edition)").FirstOrDefault();

            if (originalGoArrow != null && vvsGoArrow != null) {
                errors.Add($"You have both the original GoArrow and GoArrow (VVS Edition) installed. You should remove one of them.");
                return false;
            }

            Console.WriteLine($"Passed: GoArrowCheck.CheckGoArrowConflicts");

            return true;
        }
    }
}
