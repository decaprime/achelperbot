﻿using ACHelperBot.Lib;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace ACHelperBot.Models {
    enum DecalComponentType {
        Plugin,
        NetworkFilter,
        Service,
        Surrogate,
        InputAction
    }

    class DecalExport {
        private static char[] linebreaks = new[] { '\r', '\n' };

        /// <summary>
        /// The full text of the decal export
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// The full text of the decal export split into lines, with blanks removed
        /// </summary>
        public string[] Lines { get; private set; }


        /// <summary>
        /// Decal version
        /// </summary>
        public Version DecalVersion { get; private set; }

        /// <summary>
        /// List of all registered decal components
        /// </summary>
        public List<DecalComponent> Components { get; private set; } = new List<DecalComponent>();

        /// <summary>
        /// List of all registered decal plugins
        /// </summary>
        public List<DecalComponent> Plugins {
            get { return Components.Where(c => c.ComponentType == DecalComponentType.Plugin).ToList(); }
        }

        /// <summary>
        /// List of all registered decal network filters
        /// </summary>
        public List<DecalComponent> NetworkFilters {
            get { return Components.Where(c => c.ComponentType == DecalComponentType.NetworkFilter).ToList(); }
        }

        /// <summary>
        /// List of all registered decal services
        /// </summary>
        public List<DecalComponent> Services {
            get { return Components.Where(c => c.ComponentType == DecalComponentType.Service).ToList(); }
        }

        /// <summary>
        /// List of all registered decal surrogates
        /// </summary>
        public List<DecalComponent> Surrogates {
            get { return Components.Where(c => c.ComponentType == DecalComponentType.Surrogate).ToList(); }
        }

        /// <summary>
        /// List of all registered decal input actions
        /// </summary>
        public List<DecalComponent> InputActions {
            get { return Components.Where(c => c.ComponentType == DecalComponentType.InputAction).ToList(); }
        }

        /// <summary>
        /// True if the user has the location checkbox enabled on export
        /// </summary>
        public bool HasPluginLocationsEnabled { get; private set; }

        /// <summary>
        /// OPerating system string, like:
        /// `Microsoft Windows 7 Service Pack 1 (Build 7601)` or `(Build 9200)`
        /// </summary>
        public string OperatingSystem { get; private set; }

        /// <summary>
        /// True if user is part of the windows Administrator group
        /// </summary>
        public bool UserIsInAdministratorsGroup { get; private set; }

        /// <summary>
        /// True if this decal export was able to be parsed
        /// </summary>
        public bool DidParse {
            get {
                return string.IsNullOrEmpty(ParseError);
            }
        }

        /// <summary>
        /// Parse error, if any
        /// </summary>
        public string ParseError { get; private set; }

        public DecalSettings DecalSettings { get; private set; }
        public D3DXLibraries D3DXLibraries { get; private set; }
        public DotNetFrameworks DotNetFrameworks { get; private set; }

        public DecalExport(string exportContents) {
            Lines = exportContents.Split(linebreaks, StringSplitOptions.RemoveEmptyEntries);

            if (!LooksLikeDecalExport())
                return;

            TryParse();
        }

        /// <summary>
        /// prints out some basic details to console, for debugging
        /// </summary>
        internal void Print() {
            Console.WriteLine($"Decal Version: {DecalVersion}");
            Console.WriteLine($"Has Locations Exported: {HasPluginLocationsEnabled}");
            Console.WriteLine($"Components: ({Plugins.Count}):");

            foreach (var component in Components) {
                // component.Print();
            }

            Console.WriteLine($"Operating System: {OperatingSystem}");
            Console.WriteLine($"User Is In Administrators Group: {UserIsInAdministratorsGroup}");

            DecalSettings.Print();
            D3DXLibraries.Print();
            DotNetFrameworks.Print();
        }

        /// <summary>
        /// try to parse the decal export
        /// </summary>
        private void TryParse() {
            for (int i = 0; i < Lines.Length; i++) {
                string line = Lines[i];

                // decal version header
                if (line.Contains("DenAgent.exe")) {
                    ParseDecalVersion(line);
                    continue;
                }
                // list of plugins/filters/etc
                else if (line.StartsWith("type, enabled, name,")) {
                    i = ParsePluginList(i);
                }
                else if (line.StartsWith("Operating System:")) {
                    i = ParseOperatingSystem(i);
                }
                else if (line.StartsWith("[User is member of Administrators group]")) {
                    UserIsInAdministratorsGroup = ParseKV_Value(line).ToLower() == "true";
                }
                else if (line == "Decal Settings") {
                    DecalSettings = new DecalSettings();
                    i = DecalSettings.Parse(Lines, i);
                }
                else if (line.StartsWith("D3DX Libraries")) {
                    D3DXLibraries = new D3DXLibraries();
                    i = D3DXLibraries.Parse(Lines, i);
                }
                else if (line == ".NET Frameworks") {
                    DotNetFrameworks = new DotNetFrameworks();
                    i = DotNetFrameworks.Parse(Lines, i);
                }
            }
        }

        private string ParseKV_Value(string line) {
            var parts = line.Split(" : ");

            return parts.Length == 2 ? parts[1] : "";
        }

        /// <summary>
        /// Parses the decal version from the header
        /// </summary>
        /// <param name="line"></param>
        private void ParseDecalVersion(string line) {
            DecalVersion = new Version(line.Split(' ').Last());
        }

        /// <summary>
        /// Parses the list of plugins/filters/etc
        /// </summary>
        /// <returns></returns>
        private int ParsePluginList(int currentLine) {
            var headers = Lines[currentLine].Split(',').Select(h => h.Trim()).Where(h => !string.IsNullOrEmpty(h)).ToArray();

            if (headers.Contains("location"))
                HasPluginLocationsEnabled = true;

            // from reading the plugin list header
            currentLine++;

            while (currentLine < Lines.Length) {
                var parts = Lines[currentLine].Split(',');
                var type = parts[0];
                var shouldBreak = false;

                switch (type) {
                    case "Plugins":
                        TryAddComponent(DecalComponentType.Plugin, headers, parts);
                        break;

                    case "Network Filters":
                        TryAddComponent(DecalComponentType.NetworkFilter, headers, parts);
                        break;

                    case "Services":
                        TryAddComponent(DecalComponentType.Service, headers, parts);
                        break;

                    case "Surrogates":
                        TryAddComponent(DecalComponentType.Surrogate, headers, parts);
                        break;

                    case "Input Actions":
                        TryAddComponent(DecalComponentType.InputAction, headers, parts);
                        break;

                    default:
                        shouldBreak = true;
                        break;
                }

                if (shouldBreak)
                    break;
                else
                    currentLine++;
            }

            return currentLine - 1;
        }

        /// <summary>
        /// Grabs the operating system line text
        /// </summary>
        /// <param name="currentLine"></param>
        /// <returns></returns>
        private int ParseOperatingSystem(int currentLine) {
            currentLine++;
            OperatingSystem = Lines[currentLine].Trim();

            return currentLine;
        }

        /// <summary>
        /// Attempts to add a registered decal component
        /// </summary>
        /// <param name="cType"></param>
        /// <param name="headers"></param>
        /// <param name="parts"></param>
        private void TryAddComponent(DecalComponentType cType, string[] headers, string[] parts) {
            if (headers.Length == parts.Length) {
                Components.Add(new DecalComponent(cType, headers, parts));
            }
            else {
                Console.WriteLine($"Headers length did not match Parts length: ({headers.Length} != {parts.Length})");
                Console.WriteLine("Headers: " + string.Join(", ", headers));
                Console.WriteLine("Parts: " + string.Join(", ", parts));
            }
        }

        /// <summary>
        /// Checks to see if this text *looks like* a decal export. Note: this does not mean it is error free, just that it
        /// looks like a decal export
        /// </summary>
        /// <returns>true if this appears to be a valid export.</returns>
        private bool LooksLikeDecalExport() {
            if (Lines.Length <= 50) {
                ParseError = $"Only had {Lines.Length} lines, too short";
                return false;
            }

            if (!Lines[0].Contains("DenAgent.exe) version:")) {
                ParseError = "Invalid header. Expected: Decal (DenAgent.exe) version:";
                return false;
            }

            return true;
        }
    }
}
