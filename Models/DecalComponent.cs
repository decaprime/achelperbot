﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACHelperBot.Models {
    /*
        type, enabled, name, version, clsid
        Plugins, 0, Virindi Chat System 5, 5.0.0.5, {0A7B25AC-79A9-4F6A-9751-419B87A7BB05}
        Network Filters, 1, Echo Filter 2, 2.9.7.5, {34239EAD-6317-4C40-A405-193BA5232DD8}
        Services, 1, Decal Dat Service, 2.9.7.5, {37B083F0-276E-43AD-8D26-3F7449B519DC}
        Surrogates, -1, Version 1 Plugin Surrogate, 2.9.7.5, {3D837F6E-B5CA-4604-885F-7AB45FCFA62A}
        Input Actions, 1, Event Input Action, 2.9.7.5, {D6E4BD19-4900-4515-BCE2-A9EA4AAE2699}
     */

    class DecalComponent {

        /// <summary>
        /// The type of this decal component, like plugin/filter/etc
        /// </summary>
        public DecalComponentType ComponentType { get; private set; }

        /// <summary>
        /// Is this component enabled in decal. 1 = Yes, 0 = No, -1 = Error (-1 is ok for plugin surrogate ONLY)
        /// </summary>
        public int Enabled { get; private set; }

        /// <summary>
        /// Name of this component
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Version of this component
        /// </summary>
        public Version Version { get; private set; }

        /// <summary>
        /// ClassId of this component
        /// </summary>
        public string ClassId { get; private set; }

        /// <summary>
        /// Location of this assembly
        /// </summary>
        public string Location { get; private set; }

        /// <summary>
        /// True if NoDLL error was present
        /// </summary>
        public bool NoDll { get; private set; }

        /// <summary>
        /// True if NoVersion error was present
        /// </summary>
        public bool NoVersion { get; private set; }

        /// <summary>
        /// True if ErrorLoadingDLLPath error was present
        /// </summary>
        public bool ErrorLoadingDLLPath { get; private set; }

        public DecalComponent(DecalComponentType cType, string[] headers, string[] parts) {
            ComponentType = cType;

            for (var i = 0; i < headers.Length; i++) {
                var header = headers[i].ToLower().Trim();
                var part = parts[i].Trim();

                switch (header) {
                    case "enabled":
                        if (Int32.TryParse(part, out int result))
                            Enabled = result;
                        break;
                    case "name":
                        Name = part;
                        break;
                    case "version":
                        if (part == "<No Version>")
                            NoVersion = true;
                        else
                            try {
                                Version = new Version(part);
                            }
                            catch { }
                        break;
                    case "location":
                        if (part == "<Error Loading DLL Path>")
                            ErrorLoadingDLLPath = true;
                        else if (part == "<No DLL>")
                            NoDll = true;
                        else
                            Location = part;
                        break;
                    case "clsid":
                        ClassId = part;
                        break;
                }
            }
        }

        public void Print() {
            Console.WriteLine($"\t{Name} [{Enabled}]: {Version}");
        }
    }
}
